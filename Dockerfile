From java:openjdk-8u45-jdk
MAINTAINER rotem_jackoby@daytwo.com
EXPOSE  8080
CMD java -jar my-spring-boot-app-0.1.0.jar
ADD build/libs/my-spring-boot-app-0.1.0.jar .